//
//  ViewController.m
//  Example
//
//  Created by sujeking on 2022/7/30.
//

#import "ViewController.h"
#import <CDIconModule/CDIconModule.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor orangeColor];
    
    UIImageView *imv =[[UIImageView alloc] initWithFrame:(CGRect){110,110,100,100}];
    imv.image = [UIImage iconWithInfo:TBCityIconInfoMake(@"\U0000e73d", 30, [UIColor greenColor])];
    [self.view addSubview:imv];
    
}


@end
