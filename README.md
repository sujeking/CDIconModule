## 使用说明


把图标文件添加进工程

iconfont.ttf


设置info.plist

```
    <key>UIAppFonts</key>
    <array>
        <string>DIN-Medium.otf</string>
        <string>iconfont.ttf</string>
    </array>
```

使用

```
#import <CDIconModule/CDIconModule.h>

UIImageView *imv =[[UIImageView alloc] initWithFrame:(CGRect){110,110,100,100}];

imv.image = [UIImage iconWithInfo:TBCityIconInfoMake(@"\U0000e73d", 30, [UIColor greenColor])];

[self.view addSubview:imv];


// UILabel

self.backlabel.font = [UIFont fontWithName:@"iconfont" size:20];
self.backlabel.text = @"\U0000e73d";


```
